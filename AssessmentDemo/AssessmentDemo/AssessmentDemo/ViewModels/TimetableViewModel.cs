﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class TimetableViewModel : HelperViewModel
    {
        private TimetableModel _FacebookModel { get; set; }

        public TimetableViewModel()
        {
            _FacebookModel = new TimetableModel
            {
                BannerImageSource = BannerImageSource
            };
        }
    }
}
