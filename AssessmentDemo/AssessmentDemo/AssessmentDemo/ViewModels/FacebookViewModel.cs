﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class FacebookViewModel : HelperViewModel
    {
        private FacebookModel _FacebookModel { get; set; }

        public FacebookViewModel()
        {
            _FacebookModel = new FacebookModel
            {
                BannerImageSource = BannerImageSource
            };
        }

    }
}
