﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class YoutubeViewModel : HelperViewModel
    {
        private YoutubeModel _YoutubeViewModel { get; set; }

        public YoutubeViewModel()
        {
            _YoutubeViewModel = new YoutubeModel
            {
                BannerImageSource = BannerImageSource
            };
        }
    }
}
