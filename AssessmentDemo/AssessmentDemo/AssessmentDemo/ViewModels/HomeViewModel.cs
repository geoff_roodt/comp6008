﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class HomeViewModel : HelperViewModel
    {
        private HomeModel _HomeModel { get; set; }

        public HomeViewModel()
        {
            _HomeModel = new HomeModel
            {
                BannerImageSource = BannerImageSource
            };
        }

    }
}
