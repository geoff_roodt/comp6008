﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class AccountViewModel : HelperViewModel
    {
        private AccountModel _account { get; }

        public bool SignedIn => _account.SignedIn;

        public string ProfilePicture => _account.ProfilePicture;

        public string ProfileName => _account.ProfileName;

        public AccountViewModel()
        {
            _account = new AccountModel();
        }

        public bool SignIn(string Username, string Password)
        {
            // call api to try and sign in
            SetUsername(Username);
            SetPassword(Password);

            _account.SignedIn = true;

            return true;
        }

        public void SignOut()
        {
            SetUsername("");
            SetPassword("");
            _account.SignedIn = false;
        }

        private void SetUsername(string Username)
        {
            _account.Username = Username;
        }

        private void SetPassword(string Password)
        {
            _account.Password = Password;
        }

        public void UpdateAccount(string Source, string Name)
        {
            _account.ProfilePicture = string.IsNullOrWhiteSpace(Source) ? ProfilePicture : Source;
            _account.ProfileName = string.IsNullOrWhiteSpace(Name) ? ProfileName : Name;
        }
    }
}
