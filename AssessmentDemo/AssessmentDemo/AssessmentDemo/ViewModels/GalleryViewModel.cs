﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using AssessmentDemo.Models;
using Xamarin.Forms;
using MvvmHelpers;

namespace AssessmentDemo.ViewModels
{
    public class GalleryViewModel : BaseViewModel
    {
        ICommand _previewImageCommand = null;
        ObservableCollection<GalleryModel> _images = new ObservableCollection<GalleryModel>();
        ImageSource _previewImage = null;

        public GalleryViewModel()
        {
        }

        public void AddImages()
        {
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Bakery_Thumb.png"), OrgImage = ImageSource.FromFile("Bakery.jpg")});
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Campus01_Thumb.png"), OrgImage = ImageSource.FromFile("Campus01.jpg")});
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Campus02_Thumb.png"), OrgImage = ImageSource.FromFile("Campus02.jpg") });
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Campus03_Thumb.png"), OrgImage = ImageSource.FromFile("Campus03.jpg") });
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Bakery_Thumb.png"), OrgImage = ImageSource.FromFile("Bakery.jpg") });
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Campus01_Thumb.png"), OrgImage = ImageSource.FromFile("Campus01.jpg") });
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Campus02_Thumb.png"), OrgImage = ImageSource.FromFile("Campus02.jpg") });
            _images.Add(new GalleryModel { Source = ImageSource.FromFile("Campus03_Thumb.png"), OrgImage = ImageSource.FromFile("Campus03.jpg") });

            PreviewImage = _images.FirstOrDefault()?.OrgImage;
        }

        public ObservableCollection<GalleryModel> Images
        {
            get { return _images; }
        }

        public ImageSource PreviewImage
        {
            get { return _previewImage; }
            set { SetProperty(ref _previewImage, value); }
        }

        public ICommand PreviewImageCommand
        {
            get
            {
                return _previewImageCommand ?? new Command<Guid>((img) =>
                       {
                           PreviewImage = _images.Single(x => x.ImageId == img).OrgImage;
                       });
            }
        }
    }
}
