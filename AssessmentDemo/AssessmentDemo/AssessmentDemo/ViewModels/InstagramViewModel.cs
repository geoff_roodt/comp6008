﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class InstagramViewModel : HelperViewModel
    {
        private InstagramModel _InstagramModel { get; set; }

        public InstagramViewModel()
        {
            _InstagramModel = new InstagramModel
            {
                BannerImageSource = BannerImageSource
            };
        }
    }
}
