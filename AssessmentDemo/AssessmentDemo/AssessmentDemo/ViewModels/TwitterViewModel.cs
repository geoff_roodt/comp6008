﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;

namespace AssessmentDemo.ViewModels
{
    public sealed class TwitterViewModel : HelperViewModel
    {
        private TwitterModel _TwitterViewModel { get; set; }

        public TwitterViewModel()
        {
            _TwitterViewModel = new TwitterModel
            {
                BannerImageSource = BannerImageSource
            };
        }
    }
}
