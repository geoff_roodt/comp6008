﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.Models;
using AssessmentDemo.Views;

namespace AssessmentDemo.ViewModels
{
    public class MasterViewModel
    {
        private bool _hasTimeTable = false;

        public ObservableCollection<MasterModel> MasterModelItems;

        private MasterModel _timetableMasterMode;

        public MasterViewModel()
        {
            MasterModelItems = new ObservableCollection<MasterModel>
            {
                new MasterModel
                {
                    Title = "Home",
                    IconSource = "tom_icon.png",
                    TargetType = typeof(HomePage)
                },
                new MasterModel
                {
                    Title = "Facebook",
                    IconSource = "fb_icon.png",
                    TargetType = typeof(FacebookPage)
                },
                new MasterModel
                {
                    Title = "Instagram",
                    IconSource = "instagram_icon.png",
                    TargetType = typeof(InstagramPage)
                }
                ,
                new MasterModel
                {
                    Title = "Twitter",
                    IconSource = "twitter_icon.png",
                    TargetType = typeof(TwitterPage)
                },
                new MasterModel
                {
                    Title = "Youtube",
                    IconSource = "youtube_icon.png",
                    TargetType = typeof(YoutubePage)
                }
            };

            _timetableMasterMode = new MasterModel
            {
                Title = "TimeTable",
                IconSource = "timetable_icon.png",
                TargetType = typeof (TimetablePage)
            };
        }
        
        public void AddTimeTable()
        {
            if (!_hasTimeTable)
            {
                _hasTimeTable = true;
                MasterModelItems.Add(_timetableMasterMode);
            }
        }

        public void RemoveTimeTable()
        {
            if (_hasTimeTable)
            {
                MasterModelItems.Remove(_timetableMasterMode);
                _hasTimeTable = false;
            }
        }
    }
}
