﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AssessmentDemo.Logic;
using AssessmentDemo.Models;
using AssessmentDemo.ViewModels;
using Xamarin.Forms;

namespace AssessmentDemo
{
    public partial class App : Application
    {
        private Color _originalPageBackgroundColor;

        public static AccountViewModel Account { get; private set; }

        public App()
        {
            InitializeComponent();

            Account = new AccountViewModel();
            MainPage = new MainPage();
            ((MasterDetailPage)MainPage).IsPresentedChanged += OnIsPresentedChanged;

        }

        private async void OnIsPresentedChanged(object Sender, EventArgs Args)
        {
            var MasterDetailPage = (MasterDetailPage)MainPage;

            if (MasterDetailPage.IsPresented)
            {
                var CurrentPage = MasterDetailPage.Detail;
                _originalPageBackgroundColor = CurrentPage.BackgroundColor;

                CurrentPage.BackgroundColor = Color.Black;
                await CurrentPage.FadeTo(0.5);
            }
            else
            {
                var CurrentPage = MasterDetailPage.Detail;
                CurrentPage.BackgroundColor = _originalPageBackgroundColor;
                await CurrentPage.FadeTo(1.0);
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
