﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssessmentDemo.ViewModels;
using Xamarin.Forms;

namespace AssessmentDemo
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            
            var homeViewModel = new HomeViewModel();
            var galleryViewModel = new GalleryViewModel();

            imgBanner.Source = homeViewModel.BannerImageSource;
            BindingContext = galleryViewModel;
            galleryViewModel.AddImages();

        }
    }
}
