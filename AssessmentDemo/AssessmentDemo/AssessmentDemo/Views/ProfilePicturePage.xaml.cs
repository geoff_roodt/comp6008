﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace AssessmentDemo.Views
{
    public partial class ProfilePicturePage : ContentPage
    {
        private bool _navigatingAway;

        public ProfilePicturePage()
        {
            InitializeComponent();

            BindingContext = App.Account;

            var ProfileTap = new TapGestureRecognizer();
            ProfileTap.Tapped += ProfileTap_Tapped;
            imgProfile.GestureRecognizers.Add(ProfileTap);
        }

        #region events

        private async void ProfileTap_Tapped(object sender, EventArgs e)
        {
            // access images on phone
        }

        private async void BtnAction_OnClicked(object Sender, EventArgs E)
        {
            if (!_navigatingAway)
            {
                _navigatingAway = true;
                App.Account.UpdateAccount(App.Account.ProfilePicture, entProfileName.Text);
                await Navigation.PopModalAsync();
            }
        }

        #endregion
    }
}
