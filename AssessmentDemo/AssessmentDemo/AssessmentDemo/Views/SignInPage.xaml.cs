﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace AssessmentDemo.Views
{
    public partial class SignInPage : ContentPage
    {
        private bool _navigatingAway;

        public SignInPage()
        {
            InitializeComponent();
        }

        private async void BtnSignIn_OnClicked(object Sender, EventArgs E)
        {
            // assuming validation passes.
            // go back
            if (!_navigatingAway && App.Account.SignIn(entUsername.Text, entPassword.Text))
            {
                _navigatingAway = true;
                await Navigation.PopModalAsync(true);
            }
        }
    }
}
