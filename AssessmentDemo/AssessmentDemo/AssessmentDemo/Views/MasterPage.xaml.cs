﻿using System;

using Xamarin.Forms;

using AssessmentDemo.Views;
using AssessmentDemo.ViewModels;
using AssessmentDemo.Interfaces;

namespace AssessmentDemo
{
    public partial class MasterPage : ContentPage
    {
        private bool _navigatingAway;
        private MasterViewModel _masterViewModel;
        public ListView ListView { get { return listView; } }

        public MasterPage()
        {
            InitializeComponent();

            _masterViewModel = new MasterViewModel();
            listView.ItemsSource = _masterViewModel.MasterModelItems;
            imgProfile.BindingContext = App.Account;

            var ProfileTap = new TapGestureRecognizer();
            ProfileTap.Tapped += ProfileTap_Tapped;
            imgProfile.GestureRecognizers.Add(ProfileTap);
        }

        #region overrides

        protected override void OnAppearing()
        {
            base.OnAppearing();

            SetActionText();

            _navigatingAway = false;

            if (App.Account.SignedIn)
            {
                SignedIn();
            }
        }

        #endregion

        #region events
        
        private async void BtnAction_OnClicked(object Sender, EventArgs E)
        {
            if (!_navigatingAway && App.Account.SignedIn)
            {
                SignOut();
            }
            else if(!_navigatingAway)
            {
                _navigatingAway = true;
                await Navigation.PushModalAsync(new SignInPage(), true);
            }
        }
        
        private async void ProfileTap_Tapped(object sender, EventArgs e)
        {
            if (!_navigatingAway && App.Account.SignedIn)
            {
                _navigatingAway = true; 
                await Navigation.PushModalAsync(new ProfilePicturePage(), true);
            }
        }

        #endregion

        #region private methods

        private void SignedIn()
        {
            _masterViewModel.AddTimeTable();
            imgProfile.IsVisible = true;
        }

        private void SignOut()
        {
            App.Account.SignOut();
            SetActionText();
            imgProfile.IsVisible = false;
            _masterViewModel.RemoveTimeTable();
            ((IMainPageHelper)App.Current.MainPage).NavigateHome();
        }

        private void SetActionText()
        {
            btnAction.Text = App.Account.SignedIn ? "Sign Out" : "Sign In";
        }

        #endregion

    }
}
