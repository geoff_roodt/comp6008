﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using RestSharp;

namespace AssessmentDemo.Logic
{
    public static class APIHelper
    {
        private static readonly string URL = "http://localhost:57316/api/";

        public static bool IsConnected
        {
            get
            {
                return false;
            }
        }

        #region request

        public static RestRequest GetRestRequest(string Resource, Method Method)
        {
            return new RestRequest(Resource, Method) { RequestFormat = DataFormat.Json };
        }

        #endregion

        #region response

        private static RestClient GetRestClient()
        {
            return new RestClient(URL);
        }

        private static async Task<T> GetRestResponse<T>(RestRequest MyRequest, CancellationToken Token)
        {
            var MyClient = GetRestClient();
            var MyResponse = await MyClient.GetResponseAsync(MyRequest, Token);

            Token.ThrowIfCancellationRequested();

            var Result = JsonConvert.DeserializeObject<T>(MyResponse.Content);
            return Result;
        }

        #endregion


    }

    public class ApiResult
    {
        public bool status { get; set; }
        public string result { get; set; }
    }
}
