﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using RestSharp;

namespace AssessmentDemo.Logic
{
    public static class RestClientExtensions
    {
        private static async Task<T> SelectAsync<T>(this RestClient client, IRestRequest request, Func<IRestResponse, T> selector, CancellationToken Token)
        {
            var tcs = new TaskCompletionSource<T>();
            HttpStatusCode MyResponseStatusCode;
            string MyResponseStatusText;

            var MyHandle = client.ExecuteAsync(request, r =>
            {
                if (r.ErrorException == null)
                {
                    MyResponseStatusCode = r.StatusCode;
                    MyResponseStatusText = r.StatusDescription;
                    tcs.SetResult(selector(r));
                }
                else
                {
                    tcs.SetException(r.ErrorException);
                }
            });

            using (Token.Register(MyHandle.Abort))
            {
                return await tcs.Task;
            }
        }

        public static Task<string> GetContentAsync(this RestClient client, IRestRequest request, CancellationToken Token)
        {
            return client.SelectAsync(request, r => r.Content, Token);
        }

        public static Task<IRestResponse> GetResponseAsync(this RestClient client, IRestRequest request, CancellationToken Token)
        {
            return client.SelectAsync(request, r => r, Token);
        }
    }
}
