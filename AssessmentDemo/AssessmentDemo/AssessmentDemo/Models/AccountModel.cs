﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssessmentDemo.Models
{
    public class AccountModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public bool SignedIn { get; set; }

        public string ProfilePicture { get; set; } = "profile_icon.png";

        public string ProfileName { get; set; } = "user";
    }
}
