﻿using System;

namespace AssessmentDemo.Models
{
    public class MasterModel
    {
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type TargetType { get; set; }
    }
}
