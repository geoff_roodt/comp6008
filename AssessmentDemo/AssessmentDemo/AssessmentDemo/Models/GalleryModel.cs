﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using MvvmHelpers;

namespace AssessmentDemo.Models
{
    public class GalleryModel : ObservableObject
    {
        public Guid ImageId { get; set; }

        public ImageSource Source { get; set; }

        public ImageSource OrgImage { get; set; }

        public GalleryModel()
        {
            ImageId = Guid.NewGuid();

        }


    }
}
