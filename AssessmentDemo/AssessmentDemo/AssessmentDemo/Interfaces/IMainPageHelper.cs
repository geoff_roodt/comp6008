﻿namespace AssessmentDemo.Interfaces
{
    interface IMainPageHelper
    {
        void NavigateHome();
    }
}
