﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using AssessmentDemo.Interfaces;
using AssessmentDemo.Models;

namespace AssessmentDemo
{
    public partial class MainPage : MasterDetailPage, IMainPageHelper
    {
        private MasterDetailState _MasterDetailState;

        public MainPage()
        {
            InitializeComponent();
            _MasterDetailState = new MasterDetailState();
                        
            masterPage.ListView.ItemSelected += OnItemSelected;

            if (Device.OS == TargetPlatform.Windows)
            {
                Master.Icon = "swap.png";
            }
            else
            {
                Master.Icon = "three_lines.png";
            }
        }

        #region IMainPageHelper

        public void NavigateHome()
        {
            if (!_MasterDetailState.SameTargetPage("Home"))
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(HomePage)));
                _MasterDetailState.LastPage = "Home";
            }

            masterPage.ListView.SelectedItem = null;
            IsPresented = false;
        }

        #endregion

        #region events

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterModel;
            if (item != null)
            {
                if (!_MasterDetailState.SameTargetPage(item.Title))
                {
                    Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                    _MasterDetailState.LastPage = item.Title;
                }

                masterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
        }

        #endregion

    }

    public class MasterDetailState
    {
        public string LastPage { get; set; }

        public MasterDetailState()
        {
            LastPage = "Home";
        }

        public bool SameTargetPage(string TargetPage)
        {
            return TargetPage.ToLower() == LastPage.ToLower();
        }

    }
}
