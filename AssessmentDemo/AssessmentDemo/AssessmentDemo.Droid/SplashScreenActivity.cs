using System.Threading;
using Android.App;
using Android.OS;

namespace AssessmentDemo.Droid
{
    [Activity(Label = "Assessment Demo", Theme="@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashScreenActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Thread.Sleep(1000);
            StartActivity(typeof(MainActivity));
        }
    }
}